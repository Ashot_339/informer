#! /bin/bash/
if [ -n '`apt-cache policy figlet | grep "none"`' ]
then
sudo apt-get install figlet
fi
if [ -n '`apt-cache mesa-utils | grep "none"`' ]
then
sudo apt-get install mesa-utils
fi
echo -e "\033[34mWelcome to all-around informer.\nPress 1 for Central Processor Unit(CPU) information.\nPress 2 for information about Random Acces Meamory(RAM).\nPress 3 for information about Operation System(OS).\nPress 4 for Checking Wi-Fi connection.\nPress 5 for information about Hard Disc Drive(HDD).\nPress 6 to show Display characters.\nPress 7 to show LAN connections.\nPress 8 to show all enumbered information\033[0m"
read NUM
if [ $NUM -eq 1 ] #For Processor
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  CPU  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`lscpu | grep "Model"`\033[0m"
echo -e "\033[36m`lscpu | grep "CPU MHz"`echo -e "
echo -e "\033[36m`lscpu | grep "CPU family"`\033[0m"
echo -e "\033[36m`lscpu | grep "CPU op-mode"`\033[0m"
echo -e "\033[36m`lscpu | grep "Architecture"`\033[0m"
echo -e "\033[36m`lscpu | grep "Vendor"`\033[0m"
elif [ $NUM -eq 2 ] #For RAM
then 
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  RAM  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`cat /proc/meminfo | head -4`\033[0m"
elif [ $NUM -eq 3 ] #For OS
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  (OS)  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`uname -a`\033[0m"
elif [ $NUM -eq 4 ] #For Network
then
if [ -n '`ifconfig | grep "wlan"`' ] #For WI-FI
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  WI-FI  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[32mYou are connected to Wireless Network\033[0m"
else
echo -e "\033[31mYou are disconnected from Wireless Network\033[0m"
fi
elif [ $NUM -eq 5 ] #For HDD
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  HDD  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`lsblk | head -6`\033[0m"
elif [ $NUM -eq 6 ] #For Display
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  (DISP.)  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`xrandr | head -2`\033[0m"
echo -e "\033[36m`glxinfo | grep vendor`\033[0m"
echo -e "\033[36m`lspci | grep VGA`\033[0m"
echo -e "\033[36m`glxinfo | grep direct `\033[0m"
elif [ $NUM -eq 7 ] #LAN connection members
then
SUM=0 #Displays number of LAN connection members
for i in `seq 1 255`
do
if [ -n "`ping -c 1 192.168.1.$i | grep "rtt"`" ]
then
echo -e "\033[36mIP address of $i-th device      192.168.1.$i\033[0m"
SUM=`expr $SUM + 1`
fi
done
echo -e "\033[36mThere are $SUM connected members in your LAN connection\033[0m"
elif [ $NUM -eq 8 ] #All-around
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  CPU  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`lscpu | grep "Model"`\033[0m"
echo -e "\033[36m`lscpu | grep "CPU MHz"`echo -e "
echo -e "\033[36m`lscpu | grep "CPU family"`\033[0m"
echo -e "\033[36m`lscpu | grep "CPU op-mode"`\033[0m"
echo -e "\033[36m`lscpu | grep "Architecture"`\033[0m"
echo -e "\033[36m`lscpu | grep "Vendor"`\033[0m"
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  RAM  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`cat /proc/meminfo | head -4`\033[0m"
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  HDD  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`lsblk | head -6`\033[0m"
if [ -n '`ifconfig | grep "wlan"`' ]
then
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  WI-FI  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[32mYou are connected to Wireless Network\033[0m"
else
echo -e "\033[31mYou are disconnected from Wireless Network\033[0m"
fi
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  (OS)  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`uname -a`\033[0m"
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  (DISP.)  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
echo -e "\033[36m`xrandr | head -2`\033[0m"
echo -e "\033[36m`glxinfo | grep vendor`\033[0m"
echo -e "\033[36m`lspci | grep VGA`\033[0m"
echo -e "\033[36m`glxinfo | grep direct`\033[0m"
echo -e "\033[32;1;4m-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=  LAN  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\033[0m"
SUM=0 
for i in `seq 1 255`
do
if [ -n "`ping -c 1 192.168.1.$i | grep "rtt"`" ]
then
echo -e "\033[36mIP address of $i-th device      192.168.1.$i\033[0m"
SUM=`expr $SUM + 1`
fi
done
echo -e "\033[36mThere are $SUM connected members in your LAN connection\033[0m"
else
clear
echo -e "\033[2;1;4m\t\tThank You for using This All-around informer\n\t\t\tcreated by Ashot Poghosyan\n\t\t\t\t13.05.2015\033[0m"
figlet ASH POGHOSYAN
sleep 10s
reset
fi

